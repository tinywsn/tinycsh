#
# Copyright (c) 2021, www.tinywsn.net
#
#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
from builtins import bytes
import sys
import os
import argparse
import json
import struct
import mylib
import pkt
import ser
import time

# dump bits
DUMP_M_USR = 0x01
DUMP_M_PKT = 0x02
DUMP_M_ESC = 0x04
DUMP_M_TXP = 0x08

def show_data(cfg, dir, esc_data, data, wipc_pkt_detail, extra = ""):
  # decode fail
  if wipc_pkt_detail["error"]:
    mylib.dump_data(dir, "DEC_FAIL", data, extra)
    return
  # dump esc
  if res.x & DUMP_M_ESC: 
    mylib.dump_data(dir, "ESC_DATA", esc_data, extra)
  # dump pkt
  if res.x & DUMP_M_PKT: 
    mylib.dump_data(dir, "PKT_DATA", data, extra)
  # dump usr  
  wipc_msg_detail = wipc_pkt_detail["wipc_msg_detail"]
  if not "wipc_msg_usr_data.data" in wipc_msg_detail.keys():
    return
  data = wipc_msg_detail["wipc_msg_usr_data.data"]
  if res.x & DUMP_M_USR:
    mylib.dump_data(dir, "USR_DATA", data, extra)

def tx_data_qry_dsr(cfg, ser, res, data):
  # encode data
  msg = {"type": pkt.WIPC_PKT_USR_DATA, "data": data}  
  tx_data = pkt.encode(res.a, res.ctrl, msg, esc = False)
  # insert esc char
  tx_esc_data = pkt.enesc(tx_data)
  # serial tx data
  for i in range(res.n):
    # qry until tx succfullly
    while not ser.getdsr(): 
      time.sleep(cfg["QRY_INTV"])
    # tx pkt and wait proc
    ser.txpkt(tx_esc_data)
    time.sleep(cfg["PKT_WAIT"])
    # txp dump
    if res.x & DUMP_M_TXP:
      wipc_pkt_detail = pkt.decode(tx_data, esc = False)
      show_data(cfg, "TX", tx_esc_data, tx_data, wipc_pkt_detail)     
    # inc last 
    data[-1] += 1
    msg = {"type": pkt.WIPC_PKT_USR_DATA, "data": data}  
    tx_data = pkt.encode(res.a, res.ctrl, msg)
    tx_esc_data = pkt.enesc(tx_data)

def tx_data(cfg, ser, res, data):
  # flow control
  if res.q == "dsr":
    tx_data_qry_dsr(cfg, ser, res, data)
    return
  # encode data
  msg = {"type": pkt.WIPC_PKT_USR_DATA, "data": data}
  tx_data = pkt.encode(res.a, res.ctrl, msg, esc = False)
  # insert esc char
  tx_esc_data = pkt.enesc(tx_data)
  # serial tx data
  for i in range(res.n):
    ser.txpkt(tx_esc_data)
    # txp dump
    if res.x & DUMP_M_TXP:
      wipc_pkt_detail = pkt.decode(tx_data, esc = False)
      show_data(cfg, "TX", tx_esc_data, tx_data, wipc_pkt_detail)  
    # inc last 
    data[-1] += 1
    msg = {"type": pkt.WIPC_PKT_USR_DATA, "data": data}
    tx_data = pkt.encode(res.a, res.ctrl, msg, esc = False)
    esc_data = pkt.enesc(tx_data)

def do_cmd_help(cfg, ser, res):
  print("""
usage: %s -c cmd [-a addr] [-d u|d] [-s text|-v value] [-x dump] [-p port] [-b baud] -r
-a addr: dst addr
-c txd tx data
   rxd rx data
   set nvm bit
   rst nvm bit
   lod nvm bit
-s text: text
-v data: data
-x dump: dump data
   0x01 usr data
   0x02 pkt data
   0x04 esc data  
   0x08 txp data   
-p port: serial port   
-b baud: serial baud
-r rx after tx
-q mode: flow ctrl
   dsr  qry dsr
-n num: tx num   
-j ubf: pkt ctrl bit
""" % os.path.basename(sys.argv[0]))
   
def do_cmd_txd(cfg, ser, res):
  # usr data
  data = bytearray(res.s, encoding="utf-8")
  # tx data
  tx_data(cfg, ser, res, data)  
  # rx data
  if res.r: do_cmd_rxd(cfg, ser, res)
  
def do_cmd_rxd(cfg, ser, res):
  while True:
    # serial rx data
    esc_data = ser.rxpkt()
    # remove esc char
    data = pkt.unesc(esc_data)
    # decode data  
    wipc_pkt_detail = pkt.decode(data)
    # show data
    show_data(cfg, "RX", esc_data, data, wipc_pkt_detail)
    
# conf
f = open("cfg.jsn", "r")
cfg = json.load(f)
f.close()

# atoi
cfg["PKT_DUMP"] = int(cfg["PKT_DUMP"], 0)
cfg["WRT_INTV"] = float(cfg["WRT_INTV"])
cfg["WRT_WAIT"] = float(cfg["WRT_WAIT"])
cfg["WKU_TIME"] = float(cfg["WKU_TIME"])
cfg["PKT_WAIT"] = float(cfg["PKT_WAIT"])
cfg["QRY_INTV"] = float(cfg["QRY_INTV"])
 
# parse arg
parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("-h", action="store_true", default=False)
parser.add_argument("-a", type=mylib.auto_int, default=pkt.WIPC_GTWAY_ADDR)
parser.add_argument("-x", type=mylib.auto_int, default=cfg["PKT_DUMP"])
parser.add_argument("-c", default="rxd")
parser.add_argument("-s", default = "abcdefghijklmnopqrstuvwxyz0123456789")
parser.add_argument("-v", type=mylib.auto_int, default=0)
parser.add_argument("-p", default=cfg["SER_PORT"])
parser.add_argument("-b", type=mylib.auto_int, default=cfg["SER_BAUD"])
parser.add_argument("-r", action="store_true", default=False)
parser.add_argument("-q", default=cfg["QRY_MODE"])
parser.add_argument("-n", type=mylib.auto_int, default=1)
parser.add_argument("-j", default="")

try:
  res = parser.parse_args(sys.argv[1:])
except SystemExit as e:
  print(str(e))
  sys.exit(-1)

# show usage
if res.h:
  do_cmd_help(cfg, None, res)
  sys.exit(0)

# ctrl bit
res.ctrl = pkt.WIPC_C_NONE
if res.j.count("u"): res.ctrl |= pkt.WIPC_C_UP
if res.j.count("b"): res.ctrl |= pkt.WIPC_C_BCAST
if res.j.count("f"): res.ctrl |= pkt.WIPC_C_FWD
if res.j.count("n"): res.ctrl |= pkt.WIPC_C_NOD
if res.j.count("d"): res.ctrl |= pkt.WIPC_C_DTU
if res.j.count("x"): res.ctrl |= pkt.WIPC_C_NOD | pkt.WIPC_C_DTU

# get func
try:
  func = getattr(sys.modules['__main__'], 'do_cmd_' + res.c)
except AttributeError as e:
  print('cmd?: %s' % res.c)
  sys.exit(-1)

# serail
ser = ser.SerialDevice(res.p, res.b, cfg)  

# do cmd
func(cfg, ser, res)

# keep dtr and rts during tx
time.sleep(cfg["WRT_WAIT"])
ser.flush()
ser.close()
