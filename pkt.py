#
# Copyright (c) 2021, www.tinywsn.net
#
from builtins import bytes
import sys
import os
import time
import struct
import mylib

# ctrl char
CHAR_STX = 0x7d
CHAR_ETX = 0x7e
CHAR_DLE = 0x10
CHAR_ESC = 0x20

WIPC_C_NONE = 0x0000

# dir mask
WIPC_C_DIR = 0x8000

# ctrl flag
WIPC_C_UP = 0x8000
WIPC_C_OAM = 0x2000
WIPC_C_UI = 0x0400

WIPC_C_DTU = 0x0040
WIPC_C_NOD = 0x0008
WIPC_C_FWD = 0x0004
WIPC_C_BCAST = 0x0002

# pkt type
WIPC_PKT_RSP_CMD = 0x06
WIPC_PKT_CTRL_CMD = 0x0a
WIPC_PKT_USR_DATA = 0x1b
WIPC_PKT_RSP_WKUP = 0x1d
WIPC_PKT_RSP_PING = 0x21

# remap addr
WIPC_REMAP_MASK = 0xffffff00
WIPC_REMAP_ADDR = 0x7fffff00
WIPC_REMAP_PORT = 0x000000ff

# wipc addr
WIPC_LOCAL_ADDR = 0x7f000001
WIPC_GTWAY_ADDR = 0x00000001
WIPC_BCAST_ADDR = 0xffffffff

# wipc stat
WIPC_S_NONE = 0x00
WIPC_S_CONN = 0x01
WIPC_S_RXQF = 0x02
WIPC_S_RXNE = 0x04

# wipc func
WIPC_FUNC_NOD = 0
WIPC_FUNC_RTB = 1
WIPC_FUNC_RTT = 2
WIPC_FUNC_RTU = 3
WIPC_FUNC_USR = 4
WIPC_FUNC_LDR = 5

# data size
WIPC_USR_DATA_SIZE = 41

# func str
WIPC_FUNC_STR = ["NOD","RTB", "RTT", "RTU", "USR", "LDR"]

# decode packet 
def decode(data, esc = True):
  wipc_pkt_detail = {"error": None}
  wipc_msg_detail = {}
  
  # unesc data
  if esc: data = unesc(data)
  wipc_pkt_detail["packet"] = data
  # remove STX and ETX
  bpkt = bytearray(data[1:-1])
  oft = 0
  
  # len
  wipc_pkt_detail['len'] = struct.unpack_from('<B', bpkt, oft)[0]
  oft += 1
  if wipc_pkt_detail['len'] != len(bpkt):
    wipc_pkt_detail['error'] = "wipc pkt len error"
    return wipc_pkt_detail

  # cksum
  sum = 0
  for b in bpkt:
    sum += b
  sum &= 0xff  
  if sum:  
    wipc_pkt_detail['error'] = "wipc pkt cksum error"
    return wipc_pkt_detail
  
  # wipc_pkt_hdr
  wipc_pkt_detail['dst'] = struct.unpack_from('<L', bpkt, oft)[0]
  oft += 4  
  
  wipc_pkt_detail['src'] = struct.unpack_from('<L', bpkt, oft)[0]
  oft += 4  

  wipc_pkt_detail['dst2'] = struct.unpack_from('<L', bpkt, oft)[0]
  oft += 4  
  
  wipc_pkt_detail['src2'] = struct.unpack_from('<L', bpkt, oft)[0]
  oft += 4  
  
  wipc_pkt_detail['ctrl'] = struct.unpack_from('<H', bpkt, oft)[0]
  oft += 2    
  
  wipc_pkt_detail['cksum'] = struct.unpack_from('<B', bpkt, oft)[0]
  oft += 1

  wipc_pkt_detail['type'] = struct.unpack_from('<B', bpkt, oft)[0]
  oft += 1
  
  # usr data
  if wipc_pkt_detail['type'] == WIPC_PKT_USR_DATA:
    wipc_msg_detail['wipc_msg_usr_data.data'] = bpkt[oft:]
  # rsp cmd  
  elif wipc_pkt_detail['type'] == WIPC_PKT_RSP_CMD:
    wipc_msg_detail['wipc_msg_rsp_cmd.result'] = nstruct.unpack_from('<B', bpkt, oft)[0]  
    oft += 1
    wipc_msg_detail['wipc_msg_rsp_cmd.rspcmd'] = nstruct.unpack_from('<B', bpkt, oft)[0]  
    oft += 1
    wipc_msg_detail['wipc_msg_rsp_cmd.subcmd'] = nstruct.unpack_from('<B', bpkt, oft)[0]  
    oft += 1
    wipc_msg_detail['wipc_msg_rsp_cmd.subopt'] = nstruct.unpack_from('<B', bpkt, oft)[0]  
    oft += 1
    wipc_msg_detail['wipc_msg_rsp_cmd.data'] = bpkt[oft:]
  # rsp wkup
  # rsp ping
  else:
  # pkt data
    return wipc_pkt_detail
  # msg detail  
  wipc_pkt_detail['wipc_msg_detail'] = wipc_msg_detail
  return wipc_pkt_detail
  
# encode packet
def encode(dst, ctrl, msg, esc = True):
  # encode msg
  if msg["type"] == WIPC_PKT_USR_DATA:
    # wipc_msg_usr_data
    data = msg["data"][0:WIPC_USR_DATA_SIZE]
  elif msg["type"] == WIPC_PKT_CTRL_CMD:  
    # wipc_msg_ctrl_cmd
    data = msg["data"]
  else:
    return bytes()
  
  bpkt = bytearray(21 + len(data))
  oft = 0

  # wipc pkt hdr  
  struct.pack_into('<B', bpkt, oft, len(bpkt))
  oft += 1

  struct.pack_into('<L', bpkt, oft, dst)
  oft += 4

  struct.pack_into('<L', bpkt, oft, WIPC_LOCAL_ADDR)
  oft += 4

  struct.pack_into('<L', bpkt, oft, dst)
  oft += 4

  struct.pack_into('<L', bpkt, oft, WIPC_LOCAL_ADDR)
  oft += 4

  struct.pack_into('<H', bpkt, oft, ctrl)
  oft += 2

  struct.pack_into('<B', bpkt, oft, 0)
  cksum_oft = oft; oft += 1

  struct.pack_into('<B', bpkt, oft, msg["type"])
  oft += 1
  
  # msg data
  bpkt[oft:] = data
  oft += len(data)
  
  # data cksum 
  cksum = 0
  for b in bpkt:
    cksum += b
  cksum = 0x100 - (cksum & 0xff)
  cksum &= 0xff
  struct.pack_into('<B', bpkt, cksum_oft, cksum)
  
  data = bpkt
  # pkt hdr
  data.insert(0, CHAR_STX)
  data.append(CHAR_ETX)
  # pkt esc
  if esc: data = enesc(data)
  return data

# insert esc
def enesc(data):
  esc_data = [data[0]]
  for c in data[1:-1]:
    d = c 
    if d == CHAR_STX or d == CHAR_ETX or d == CHAR_DLE:
      esc_data = esc_data + [CHAR_DLE]
      d =  d ^ CHAR_ESC
    esc_data = esc_data + [d]
  return bytearray(esc_data + [data[-1]])
  
# remove esc  
def unesc(esc_data):
  escape = False
  data = []
  for c in esc_data:
    d = c
    if c == CHAR_DLE:
      escape = True
      continue
    if escape:  
      d = d ^ CHAR_ESC
      escape = False
    data = data + [d]
  if escape:
    return None
  return bytearray(data)
