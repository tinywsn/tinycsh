#
# Copyright (c) 2021, www.tinywsn.net
#
from builtins import bytes
import re
import time
import datetime
import string

def auto_int(x):
  return int(x, 0)

def auto_val(x):
  # ip addr  
  if re.match(r'[0-9]+[.][0-9]+[.][0-9]+[.][0-9]+', x):
    ip = 0
    for v in x.split("."):
      ip = ip * 256 + int(v)
    return ip  
  # float
  elif re.match(r'[+-]?[0-9]*[.]+[0-9]+', x):
    return float(x)
  else:
    return int(x, 0)

def txt2lst(txt):
  lst = []
  for c in txt:
    lst.append(ord(c))  
  return lst

def dump_data(dir, title, data, extra = ""):
  print(dir + " " + datetime.datetime.now().strftime("%H:%M:%S.%f") + " " + title + " LEN=%d" % len(data) + extra)
  oft = 0
  tot = len(data)
  while oft < tot:
    num = tot - oft
    if num > 16: num = 16
    line = "%02x|" % oft
    text = ""
    for v in data[oft:oft+num]:
      c = chr(v)
      line += " %02x" % v
      if c in string.whitespace:
        text += "."
      elif c in string.printable:
        text += c
      else:
        text += "."
    line += " " * (51 - len(line)) 
    text += " " * (16 - len(text))    
    print(line + "|" + text + "|")
    oft += num
  print  
