#
# Copyright (c) 2021, www.tinywsn.net
#
from builtins import bytes
import serial
import time
import pkt
import mylib

class SerialDevice(serial.Serial):
  def __init__(self, port, baud, cfg, **kwargs):
    serial.Serial.__init__(self, port, baud, timeout=0.1, write_timeout=1.0, **kwargs)
    self.cfg = cfg

  def read(self, size = 1):
    return serial.Serial.read(self, size)
    
  def write(self, data):
    # write all data directly
    if self.cfg["WRT_INTV"] <= 0.0:
      serial.Serial.write(self, data)
      return
    # insert wait between write
    for c in data:
      serial.Serial.write(self, c)
      time.sleep(self.cfg["WRT_INTV"])
    return
    
  def getdsr(self):
    return self.dsr
    
  def txpkt(self, data):
    # wakeup chip
    if self.cfg["WKU_WAIT"]:
      self.write([0x00]) 
      time.sleep(self.cfg["WKU_TIME"])
    else:
      self.write([0xff for i in range(self.cfg["WKU_CHRN"])]) 
    # write data
    self.write(data)
    
  def rxpkt(self):  
    data = []
    while True:
      for c in self.read():
        # python2 str into bytes
        if isinstance(c, str): c = ord(c)
        data = data + [c]
        if c == pkt.CHAR_STX: data = [c]
        if c != pkt.CHAR_ETX: continue
        try:
          i = data.index(pkt.CHAR_STX)
        except Exception as e:
          mylib.dump_data("RX", "STX Missing", bytearray(data))
          data = ""
          continue
        if i > 0: 
          mylib.dump_data("RX", "Extra Data", bytearray(data))
        return bytearray(data[i:])
