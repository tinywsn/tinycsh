# tinycsh

#### 介绍
TinyWSN 是一套SUB-1G的无线传感器网络-http://www.tinywsn.net， 
本项目是节点模块的pyserial的使用示例-http://www.tinywsn.net/wordpress/index.php/node/

#### 软件架构
本项目PC机的python脚本，它通过串口和节点模块进行消息收发，发送前它还检查节点模块的状态管脚STA_IND避免消息缓冲溢出节点，模块STA_IND和串口DSR相连

#### 安装教程
需要安装python 2.7.x环境和串口软件包pyserial (版本>=3.4）

#### 使用说明

它是一个命令行工具，例如往节点0xc0a90001 发送数据12345

dtu.py -dd -p COM49 -a 0xc0a90001 -c txd -s 12345 

具体命令选项如下：
usage: dtu.py -c cmd [-a addr] [-d u|d] [-s text|-v value] [-x dump] [-p port] [-b baud] -r

-a addr: dst addr

-d dir: direction
   u uplink
   d dnlink

-c txd tx data
   rxd rx data

-s text: text

-x dump: dump data

   0x01 usr data

   0x02 pkt data

   0x04 esc data 
 
   0x08 txp data 

-p port: serial port   

-b baud: serial baud

-r rx after tx

-q mode: flow control
   dsr  qry dsr

-n num: tx num

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
